using DotsLearn.World.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using Random = Unity.Mathematics.Random;

namespace DotsLearn.World.Mono
{
    public class WorldMono : MonoBehaviour
    {
        public float2 Dimenstions;
        public int PitsSpawnNumber;
        public uint RandomSeed;
        public float RatSpawnRate;
        public GameObject PitPrefab;  
        public GameObject RatPrefab;
    }

    public class WorldBake : Baker<WorldMono>
    {
        public override void Bake(WorldMono authoring)
        {
            var worldEntity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent(worldEntity, new WorldProperties
            {
                Dimenstions = authoring.Dimenstions,
                PitsSpawnNumber = authoring.PitsSpawnNumber,
                RatSpawnRate = authoring.RatSpawnRate,
#if !UNITY_EDITOR
                PitPrefab = GetEntity(authoring.PitPrefab, TransformUsageFlags.Dynamic),
                RatPrefab = GetEntity(authoring.RatPrefab, TransformUsageFlags.Dynamic)
#endif
            });

            AddComponent(worldEntity, new WorldRandom
            {
                Value = Random.CreateFromIndex(authoring.RandomSeed)
            });

            var builder = new BlobBuilder(Allocator.Temp);
            ref NativeArray<float3> list = ref builder.ConstructRoot<NativeArray<float3>>();

            AddBuffer<RatSpawnPointElement>(worldEntity);
            AddComponent<RatSpawnTimer>(worldEntity);

            builder.Dispose();
#if !UNITY_EDITOR
            AddComponent<PrefabReadyTag>(worldEntity);
#endif
        }
    }
}
