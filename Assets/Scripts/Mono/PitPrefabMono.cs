using DotsLearn.World.Components;
using Unity.Entities;
using Unity.Entities.Serialization;
using Unity.Scenes;
using UnityEditor;
using UnityEngine;

namespace DotsLearn.World.Mono
{
    public class PitPrefabMono : MonoBehaviour
    {
        public GameObject Prefab;
    }

#if UNITY_EDITOR
    public class PitPrefabBaker : Baker<PitPrefabMono>
    {
        public override void Bake(PitPrefabMono authoring)
        {
            DependsOn(authoring.Prefab);
            if (authoring.Prefab == null) return;

            var entity = GetEntity(TransformUsageFlags.Dynamic);

            AddComponent<PitPrefabSingleton>(entity);

            AssetDatabase.TryGetGUIDAndLocalFileIdentifier(authoring.Prefab, out string guidString, out long localID);
            GUID.TryParse(guidString, out GUID guid);
            var prefab = new EntityPrefabReference(guid);
            AddComponent(entity, new RequestEntityPrefabLoaded { Prefab = prefab });
        }
    }
#endif
}
