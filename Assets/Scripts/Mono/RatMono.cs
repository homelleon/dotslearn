using DotsLearn.World.Components;
using Unity.Entities;
using UnityEngine;

namespace DotsLearn.World.Mono
{
    public class RatMono : MonoBehaviour
    {
        public float ExitRate;
        public float RunSpeed;
        public float RunAmplitude;
        public float RunFrequency;

        public float EatDamage;
        public float EatAmplitude;
        public float EatFrequency;
    }

    public class RatBaker : Baker<RatMono>
    {
        public override void Bake(RatMono authoring)
        {
            var entity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent(entity, new RatExitRate { Value = authoring.ExitRate });
            AddComponent(entity, new RatRunProperties
            {
                RunSpeed = authoring.RunSpeed,
                RunAmplitude = authoring.RunAmplitude,
                RunFrequency = authoring.RunFrequency
            });
            AddComponent(entity, new RatEatProperties
            {
                EatDamagePerSecond = authoring.EatDamage,
                EatAmplitude = authoring.EatAmplitude,
                EatFrequency = authoring.EatFrequency
            });
            AddComponent<RatTimer>(entity);
            AddComponent<RatHeading>(entity);
            AddComponent<NewRatTag>(entity);
        }
    }
}
