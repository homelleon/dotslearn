using DotsLearn.World.Components;
using Unity.Entities;
using UnityEngine;

namespace DotsLearn.World.Mono
{
    public class CheeseMono : MonoBehaviour
    {
        public float CheeseAmount;
    }

    public class CheeseBaker : Baker<CheeseMono>
    {
        public override void Bake(CheeseMono authoring)
        {
            var entity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent<CheeseTag>(entity);
            AddComponent(entity, new CheeseAmount
            {
                Value = authoring.CheeseAmount,
                Max = authoring.CheeseAmount
            });
            AddBuffer<CheeseDamageBufferElement>(entity);
        }
    }
}
