using DotsLearn.World.Aspects;
using DotsLearn.World.Components;
using Unity.Burst;
using Unity.Entities;

namespace DotsLearn.World.Systems
{        
    [BurstCompile]
    [UpdateAfter(typeof(SpawnRatSystem))]
    public partial struct RatExitSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var deltaTime = SystemAPI.Time.DeltaTime;
            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            new RatExitJob
            {
                DeltaTime = deltaTime,
                ECB = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged).AsParallelWriter()
            }.ScheduleParallel();
        }
    }

    [BurstCompile]
    public partial struct RatExitJob : IJobEntity
    {
        public float DeltaTime;
        public EntityCommandBuffer.ParallelWriter ECB;

        [BurstCompile]
        private void Execute(RatExitAspect rat, [EntityIndexInQuery] int sortKey)
        {
            rat.Exit(DeltaTime);
            if (!rat.IsAboveGround) return;

            rat.SetAtGroundLevel();
            ECB.RemoveComponent<RatExitRate>(sortKey, rat.Entity);
            ECB.SetComponentEnabled<RatRunProperties>(sortKey, rat.Entity, true);
        }
    }

}
