using DotsLearn.World.Aspects;
using DotsLearn.World.Components;
using System.Diagnostics;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLearn.World.Systems
{
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    [UpdateAfter(typeof(EndInitializationEntityCommandBufferSystem))]
    public partial struct SpawnPitSystem : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            if (!SystemAPI.HasSingleton<PrefabReadyTag>()) return;

            var worldEntity = SystemAPI.GetSingletonEntity<WorldProperties>();

            state.Enabled = false;

            var ecb = new EntityCommandBuffer(Allocator.Temp);
            var pitOffset = new float3(0f, -2f, 1f);

            var world = SystemAPI.GetAspect<WorldAspect>(worldEntity);

            for (var i = 0; i < world.PitsSpawnNumber; i++)
            {
                var newPit = ecb.Instantiate(world.PitPrefab);
                ecb.AddComponent<RatSpawnPointTag>(newPit);
                var newPitTransform = world.GetRandomPitTransform();
                ecb.SetComponent(newPit, LocalTransform.FromMatrix(newPitTransform.ToMatrix()));
                var newRatSpawnPoint = newPitTransform.Position + pitOffset;
                world.RatSpawnPoints.Add(new RatSpawnPointElement { Value = newRatSpawnPoint });
            }

            ecb.AddComponent<PitsReadyTag>(worldEntity);

            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}