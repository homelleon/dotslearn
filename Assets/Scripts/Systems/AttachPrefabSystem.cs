using DotsLearn.World.Aspects;
using DotsLearn.World.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Scenes;

namespace DotsLearn.World.Systems
{
    [BurstCompile]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    [UpdateBefore(typeof(EndInitializationEntityCommandBufferSystem))]
    public partial struct AttachPrefabSystem : ISystem
    {
        public void OnCreate(ref SystemState state)
        {
            var worldQuery = new EntityQueryBuilder(Allocator.Persistent)
                .WithAll<WorldProperties>()
                .WithNone<PrefabReadyTag>()
                .Build(ref state);
            state.RequireForUpdate(worldQuery);
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);
            var worldEntity = SystemAPI.GetSingletonEntity<WorldProperties>();
            var world = SystemAPI.GetAspect<WorldAspect>(worldEntity);

            if (SystemAPI.HasSingleton<PitPrefabSingleton>())
            {
                var prefabSingleton = SystemAPI.GetSingletonEntity<PitPrefabSingleton>();
                if (SystemAPI.HasComponent<PrefabLoadResult>(prefabSingleton))
                {
                    var result = SystemAPI.GetComponent<PrefabLoadResult>(prefabSingleton);
                    world.PitPrefab = result.PrefabRoot;
                    ecb.RemoveComponent<PitPrefabSingleton>(prefabSingleton);
                }
            }

            if (SystemAPI.HasSingleton<RatPrefabSingleton>())
            {
                var prefabSingleton = SystemAPI.GetSingletonEntity<RatPrefabSingleton>();
                if (SystemAPI.HasComponent<PrefabLoadResult>(prefabSingleton))
                {
                    var result = SystemAPI.GetComponent<PrefabLoadResult>(prefabSingleton);
                    world.RatPrefab = result.PrefabRoot;
                    ecb.RemoveComponent<RatPrefabSingleton>(prefabSingleton);
                }
            }

            if (world.RatPrefab != Entity.Null && world.PitPrefab != Entity.Null)
                ecb.AddComponent<PrefabReadyTag>(worldEntity);

            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}
