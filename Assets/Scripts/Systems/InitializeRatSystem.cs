using DotsLearn.World.Aspects;
using DotsLearn.World.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

namespace DotsLearn.World.Systems
{
    [BurstCompile]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public partial struct InitializeRatSystem : ISystem
    { 
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);
            foreach (var rat in SystemAPI.Query<RatRunAspect>()
                .WithAll<NewRatTag>())
            {
                ecb.RemoveComponent<NewRatTag>(rat.Entity);
                ecb.SetComponentEnabled<RatRunProperties>(rat.Entity, false);
                ecb.SetComponentEnabled<RatEatProperties>(rat.Entity, false);
            }

            ecb.Playback(state.EntityManager);
        }
    }
}
