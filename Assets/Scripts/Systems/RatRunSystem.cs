using DotsLearn.World.Aspects;
using DotsLearn.World.Components;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLearn.World.Systems
{

    [BurstCompile]
    [UpdateAfter(typeof(RatExitSystem))]
    public partial struct RatRunSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            if (!SystemAPI.HasSingleton<CheeseTag>()) return;

            var deltaTime = SystemAPI.Time.DeltaTime;
            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var cheeseEntity = SystemAPI.GetSingletonEntity<CheeseTag>();
            var cheeseScale = SystemAPI.GetComponent<LocalToWorld>(cheeseEntity).Value.Scale().x;
            var cheeseRadius = cheeseScale * 0.5f + 0.5f;

            new RatRunJob
            {
                DeltaTime = deltaTime,
                CheeseRadiusSq = cheeseRadius * cheeseRadius,
                ECB = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged).AsParallelWriter()
            }.ScheduleParallel();
        }
    }

    [BurstCompile]
    public partial struct RatRunJob : IJobEntity
    {
        public float DeltaTime;
        public float CheeseRadiusSq;
        public EntityCommandBuffer.ParallelWriter ECB;
        private void Execute(RatRunAspect rat, [EntityIndexInQuery] int sortKey)
        {
            rat.Run(DeltaTime);
            if (rat.IsInStoppingRange(float3.zero, CheeseRadiusSq))
            {
                ECB.SetComponentEnabled<RatRunProperties>(sortKey, rat.Entity, false);
                ECB.SetComponentEnabled<RatEatProperties>(sortKey, rat.Entity, true);
            }
        }
    }
}
