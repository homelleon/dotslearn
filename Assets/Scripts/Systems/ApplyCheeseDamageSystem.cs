using DotsLearn.World.Aspects;
using Unity.Burst;
using Unity.Entities;

namespace DotsLearn.World.Systems
{
    [BurstCompile] 
    [UpdateInGroup(typeof(SimulationSystemGroup), OrderLast = true)]
    [UpdateAfter(typeof(EndSimulationEntityCommandBufferSystem))]
    public partial struct ApplyCheeseDamageSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            state.Dependency.Complete();
            foreach (var cheese in SystemAPI.Query<CheeseAspect>())
            {
                cheese.ApplyDamage();
            }
        }
    }
}
