using DotsLearn.World.Aspects;
using DotsLearn.World.Components;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLearn.World.Systems
{
    [BurstCompile]
    [UpdateAfter(typeof(RatRunSystem))]
    public partial struct RatEatSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            if (!SystemAPI.HasSingleton<CheeseTag>()) return;

            var deltaTime = SystemAPI.Time.DeltaTime;
            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var cheeseEntity = SystemAPI.GetSingletonEntity<CheeseTag>();
            var cheeseScale = SystemAPI.GetComponent<LocalToWorld>(cheeseEntity).Value.Scale().x;
            var cheeseRadius = cheeseScale * .5f + 1f;

            new RatEatJob
            {
                DeltaTime = deltaTime,
                ECB = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged).AsParallelWriter(),
                CheeseEntity = cheeseEntity,
                CheeseRadiusSq = cheeseRadius * cheeseRadius
            }.ScheduleParallel();
        }
    }

    [BurstCompile]
    public partial struct RatEatJob : IJobEntity
    {
        public float DeltaTime;
        public EntityCommandBuffer.ParallelWriter ECB;
        public Entity CheeseEntity;
        public float CheeseRadiusSq;

        [BurstCompile]
        private void Execute(RatEatAspect rat, [EntityIndexInQuery] int sortKey)
        {
            if (rat.IsInEatingRange(float3.zero, CheeseRadiusSq))
                rat.Eat(DeltaTime, ECB, sortKey, CheeseEntity);
            else
            {
                ECB.SetComponentEnabled<RatEatProperties>(sortKey, rat.Entity, false);
                ECB.SetComponentEnabled<RatRunProperties>(sortKey, rat.Entity, true);
            }
        }
    }
}
