using DotsLearn.World.Aspects;
using DotsLearn.World.Components;
using Unity.Burst;
using Unity.Entities;
using Unity.Transforms;

namespace DotsLearn.World.Systems
{
    [BurstCompile]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    [UpdateAfter(typeof(SpawnPitSystem))]
    public partial struct SpawnRatSystem : ISystem
    {
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<PitsReadyTag>();
            state.RequireForUpdate<CheeseTag>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var deltaTime = SystemAPI.Time.DeltaTime;
            var ecbSingleton = SystemAPI.GetSingleton<BeginInitializationEntityCommandBufferSystem.Singleton>();

            new SpawnRatJob
            {
                DeltaTime = deltaTime,
                ECB = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged)
            }.Run();
        }
    }

    [BurstCompile]
    public partial struct SpawnRatJob : IJobEntity
    {
        public float DeltaTime;
        public EntityCommandBuffer ECB;
        private void Execute(WorldAspect world)
        {
            world.RatSpawnTimer -= DeltaTime;
            if (!world.TimeToSpawnRat) return;
            if (world.RatSpawnPoints.Length == 0) return;

            world.RatSpawnTimer = world.RatSpawnRate;
            var newRat = ECB.Instantiate(world.RatPrefab);

            var newRatTransform = world.GetRatSpawnPoint();
            ECB.SetComponent(newRat, newRatTransform);
            
            var ratHeading = MathUtils.GetHeading(newRatTransform.Position, world.Position);
            ECB.SetComponent(newRat, new RatHeading { Value = ratHeading });

        }
    }
}
