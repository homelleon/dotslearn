using DotsLearn.World.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLearn.World.Aspects
{
    public readonly partial struct WorldAspect : IAspect
    {
        public readonly Entity Entity;
        private readonly RefRW<LocalTransform> _transformAspect;
        private readonly RefRW<WorldProperties> _worldProperties;
        private readonly RefRW<WorldRandom> _worldRandom;
        private readonly DynamicBuffer<RatSpawnPointElement> _ratSpawnPoints;
        private readonly RefRW<RatSpawnTimer> _ratSpawnTimer;
        public int PitsSpawnNumber => _worldProperties.ValueRO.PitsSpawnNumber;
        public Entity PitPrefab
        {
            get => _worldProperties.ValueRO.PitPrefab;
            set => _worldProperties.ValueRW.PitPrefab = value;
        }

        public DynamicBuffer<RatSpawnPointElement> RatSpawnPoints => _ratSpawnPoints;
        
        private const float CENTRAL_SAFE_DISTANCE = 100f;
        private const float MIN_PIT_SCALE = 0.2f;
        public LocalTransform GetRandomPitTransform()
        {
            return new LocalTransform
            {
                Position = GetRandomPosition(),
                Rotation = GetRandomRotation(),
                Scale = GetRandomScale(MIN_PIT_SCALE)
            };
        }
        private float3 GetRandomPosition()
        {
            float3 randomPosition;
            do
                randomPosition = _worldRandom.ValueRW.Value.NextFloat3(MinCorner, MaxCorner);
            while (math.distancesq(_transformAspect.ValueRW.Position, randomPosition) <= CENTRAL_SAFE_DISTANCE);

            return randomPosition;
        }

        private float3 MinCorner => _transformAspect.ValueRW.Position - HalfDimensions;
        private float3 MaxCorner => _transformAspect.ValueRW.Position + HalfDimensions;
        private float3 HalfDimensions => new()
        {
            x = _worldProperties.ValueRO.Dimenstions.x * 0.5f,
            y = 0f,
            z = _worldProperties.ValueRO.Dimenstions.y * 0.5f
        };

        private quaternion GetRandomRotation() => quaternion.RotateY(_worldRandom.ValueRO.Value.NextFloat(-0.25f, 0.25f));
        private float GetRandomScale(float min) => _worldRandom.ValueRO.Value.NextFloat(min, 1.0f);
    
        public float RatSpawnTimer
        {
            get => _ratSpawnTimer.ValueRO.Value;
            set => _ratSpawnTimer.ValueRW.Value = value;
        }
        public bool TimeToSpawnRat => RatSpawnTimer <= 0f;
        public float RatSpawnRate => _worldProperties.ValueRO.RatSpawnRate;
        public Entity RatPrefab
        {
            get => _worldProperties.ValueRO.RatPrefab;
            set => _worldProperties.ValueRW.RatPrefab = value;
        }
        public LocalTransform GetRatSpawnPoint()
        {
            var position = GetRandomRatSpawnPoint();
            return new LocalTransform
            {
                Position = position,
                Rotation = quaternion.RotateY(MathUtils.GetHeading(position, _transformAspect.ValueRW.Position)),
                Scale = .5f
            };
        }

        private float3 GetRandomRatSpawnPoint() =>
            RatSpawnPoints.ElementAt(_worldRandom.ValueRW.Value.NextInt(RatSpawnPoints.Length)).Value;

        public float3 Position => _transformAspect.ValueRW.Position;
    }
}