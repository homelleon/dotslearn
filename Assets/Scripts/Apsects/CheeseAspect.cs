using DotsLearn.World.Components;
using Unity.Entities;
using Unity.Transforms;

namespace DotsLearn.World.Aspects
{
    public readonly partial struct CheeseAspect : IAspect
    {
        public readonly Entity Entity;

        private readonly RefRW<LocalTransform> _transform;
        private readonly RefRW<CheeseAmount> _cheeseAmount;
        private readonly DynamicBuffer<CheeseDamageBufferElement> _cheeseDamageBuffer;

        public void ApplyDamage()
        {
            foreach (var cheeseDamageBufferElement in _cheeseDamageBuffer)
            {
                _cheeseAmount.ValueRW.Value -= cheeseDamageBufferElement.Value;
            }
            _cheeseDamageBuffer.Clear();

            _transform.ValueRW.Scale *= _cheeseAmount.ValueRO.Value / _cheeseAmount.ValueRO.Max;
        }
    }
}
