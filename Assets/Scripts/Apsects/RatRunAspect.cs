using DotsLearn.World.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLearn.World.Aspects
{
    public  readonly partial struct RatRunAspect : IAspect
    {
        public readonly Entity Entity;

        private readonly RefRW<LocalTransform> _transform;
        private readonly RefRW<RatTimer> _runTimer;
        private readonly RefRO<RatRunProperties> _runProperties;
        private readonly RefRO<RatHeading> _heading;

        private float RunSpeed => _runProperties.ValueRO.RunSpeed;
        private float RunAmplitude => _runProperties.ValueRO.RunAmplitude;
        private float RunFrequency => _runProperties.ValueRO.RunFrequency;
        private float Heading => _heading.ValueRO.Value;

        private float RunTimer
        {
            get => _runTimer.ValueRO.Value;
            set => _runTimer.ValueRW.Value = value;
        }

        public void Run(float deltaTime)
        {
            RunTimer += deltaTime;
            _transform.ValueRW.Position += _transform.ValueRW.Forward() * RunSpeed * deltaTime;

            var swayAngle = RunAmplitude * math.sin(RunFrequency * RunTimer);
            _transform.ValueRW.Rotation = quaternion.Euler(0, Heading, swayAngle);
        }

        public bool IsInStoppingRange(float3 cheesePosition, float cheeseRadiusSq)
        {
            return math.distancesq(cheesePosition, _transform.ValueRW.Position) <= cheeseRadiusSq;
        }
    }
}
