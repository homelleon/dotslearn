using DotsLearn.World.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLearn.World.Aspects
{
    public readonly partial struct RatExitAspect : IAspect
    {
        public readonly Entity Entity;
        private readonly RefRW<LocalTransform> _transform;
        private readonly RefRO<RatExitRate> _ratExitRate;

        public void Exit(float deltaTime)
        {
            _transform.ValueRW.Position += math.up() * _ratExitRate.ValueRO.Value * deltaTime;
        }

        public bool IsAboveGround => _transform.ValueRW.Position.y >= 0f;
        public void SetAtGroundLevel()
        {
            var position = _transform.ValueRW.Position;
            position.y = 0f;
            _transform.ValueRW.Position = position;
        }
    }
}
