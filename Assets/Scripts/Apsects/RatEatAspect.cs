using DotsLearn.World.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLearn.World.Aspects
{
    public  readonly partial struct RatEatAspect : IAspect
    {
        public readonly Entity Entity;
        private readonly RefRW<LocalTransform> _transform;
        private readonly RefRW<RatTimer> _ratTimer;
        private readonly RefRO<RatEatProperties> _eatProperties;
        private readonly RefRO<RatHeading> _heading;

        private float EatDamagePerSecond => _eatProperties.ValueRO.EatDamagePerSecond;
        private float EatAmplitude => _eatProperties.ValueRO.EatAmplitude;
        private float EatFrequency => _eatProperties.ValueRO.EatFrequency;
        private float Heading => _heading.ValueRO.Value;

        private float RatTimer
        {
            get => _ratTimer.ValueRO.Value;
            set => _ratTimer.ValueRW.Value = value;
        }

        public void Eat(float deltaTime, EntityCommandBuffer.ParallelWriter ecb, int sortKey, Entity cheeseEntity)
        {
            RatTimer += deltaTime;
            var eatAngle = EatAmplitude * math.sin(EatFrequency * RatTimer);
            _transform.ValueRW.Rotation = quaternion.Euler(eatAngle, Heading, 0);

            var eatDamage = EatDamagePerSecond * deltaTime;
            var curCheeseDamage = new CheeseDamageBufferElement { Value = eatDamage };
            ecb.AppendToBuffer(sortKey, cheeseEntity, curCheeseDamage);
        }

        public bool IsInEatingRange(float3 targetPosition, float targetRaidusSq)
        {
            return math.distancesq(targetPosition, _transform.ValueRW.Position) <= targetRaidusSq - 1;
        }
    }
}
