using Unity.Entities;
using Unity.Entities.Serialization;
using Unity.Mathematics;

namespace DotsLearn.World.Components
{
    public struct WorldProperties : IComponentData
    {
        public float2 Dimenstions;
        public int PitsSpawnNumber;
        public Entity PitPrefab;
        public Entity RatPrefab;
        public float RatSpawnRate;
    }
    public struct RatPrefabSingleton : IComponentData { }
    public struct PitPrefabSingleton : IComponentData { }

    public struct PrefabReadyTag : IComponentData { }
    public struct PitsReadyTag : IComponentData { }

    public struct RatSpawnTimer : IComponentData
    {
        public float Value;
    }
}
