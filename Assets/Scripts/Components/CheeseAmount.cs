using Unity.Entities;

namespace DotsLearn.World.Components
{
    public struct CheeseAmount : IComponentData
    {
        public float Value;
        public float Max;
    }
}
