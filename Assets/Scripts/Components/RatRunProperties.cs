using Unity.Entities;

namespace DotsLearn.World.Components
{
    public struct RatRunProperties : IComponentData, IEnableableComponent
    {
        public float RunSpeed;
        public float RunAmplitude;
        public float RunFrequency;
    }

    public struct RatTimer : IComponentData
    {
        public float Value;
    }

    public struct RatHeading : IComponentData
    {
        public float Value;
    }

    public struct NewRatTag : IComponentData {}
}
