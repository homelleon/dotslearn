using Unity.Entities;

namespace DotsLearn.World.Components
{
    public struct RatEatProperties : IComponentData, IEnableableComponent
    {
       public float EatDamagePerSecond;
       public float EatAmplitude;
       public float EatFrequency;
    }
}
