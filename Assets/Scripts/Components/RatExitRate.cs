using Unity.Entities;

namespace DotsLearn.World.Components
{
    public struct RatExitRate : IComponentData
    {
        public float Value;
    }
}
