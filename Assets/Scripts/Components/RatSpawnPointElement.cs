using Unity.Entities;
using Unity.Mathematics;

namespace DotsLearn.World.Components
{
    public struct RatSpawnPointElement : IBufferElementData
    {
        public float3 Value;
    }
}
