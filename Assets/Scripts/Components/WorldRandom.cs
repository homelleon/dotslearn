using Unity.Entities;
using Unity.Mathematics;

namespace DotsLearn.World.Components
{
    public struct WorldRandom : IComponentData
    {
        public Random Value;
    }
}
