using Unity.Entities;

namespace DotsLearn.World.Components
{
    public struct CheeseDamageBufferElement : IBufferElementData
    {
        public float Value;
    }
}
